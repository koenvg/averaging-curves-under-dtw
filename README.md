# Abstract

The average curve problem under the dynamic time warping distance plays an important role in analysing and clustering time series data, with applications ranging from biology and speech recognition to finances and energy.
Despite substantial effort, however, the problem resists satisfactory solutions.
Recent fundamental research on the average curve problem has given us a better understanding of the difficulties involved; NP-hardness was shown in 2019.
We strengthen this further by proving that there is no polynomial time constant factor approximation algorithm unless P = NP.
On the constructive side, we focus on the problem of finding constant length average curves.
Using techniques from real algebraic geometry, we present the first polynomial time algorithm for this problem.
We also give a more general $(1 + \epsilon)$-approximation algorithm with a better time complexity than the exact algorithm.